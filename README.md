# Consul install

## Getting started

Requirements:
- Python 3.8 and above

1. Create venv
1. Run `pip install -r requirements.txt`
1. Run `ansible-galaxy install -r requirements.yml`
1. Generate certs to `certs/` folder and gossip key for `consul_gossip_key` variable
1. Create inventory and correct `group_vars/all.yml` values
1. Run `ansible-playbook <...> docker-install.yml`
1. Run `ansible-playbook <...> consul.yml`

